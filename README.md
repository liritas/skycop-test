# Skycop test by Orangesoft

# Common information
Author: Dzmitry Sharko
Elapsed time: 7 hrs

# Instruction
Please run 'php bin/console app:parse-csv [path to CSV data file]' to execute command and view results.
Example: php bin/console app:parse-csv /data/flights.csv

# Data sources
/data
    /decision_table.csv - decision rules
    /eu_countries.csv - list of European Union countries and ISO codes
    /flights.csv - incoming data table example (transformed to valid CSV format with headers)
    /test_flights.csv - test data table

# Possible solutions
Using libraries:
    https://github.com/bobthecow/Ruler/blob/master/README.md
    https://github.com/hoaproject/Ruler/blob/master/README.md
Data engines:
    https://docs.oracle.com/middleware/1213/bpm/rules-user/GUID-4D0D2DFC-49FA-4408-A85B-3DC7AC109F39.htm#GUID-DFA21372-4498-4BC0-8E37-D7091AE4BE10
Services:
    http://openrules.com/intro.htm
    https://www.inrule.com/
Formats:
    MDG
    https://assets.cdn.sap.com/sapcom/docs/2015/07/7a293c52-5b7c-0010-82c7-eda71af511fa.pdf

# Testing
php bin/phpunit
