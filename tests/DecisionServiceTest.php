<?php
// tests/DecisionServiceTest.php
namespace App\Tests\Util;

use App\Service\DecisionService;
use PHPUnit\Framework\TestCase;

class DecisionServiceTest extends TestCase
{
    public function testAdd()
    {
        $decisionService = new DecisionService();
        $result = $decisionService->parseCSV('/data/test_flights.csv');
        $reference = [
            'LV Cancel 20 N',
            'RU Cancel 10 N',
            'LT Delay 1 N',
            'LT Delay 3 Y',
            'LV Delay 4 Y',
            'LT Cancel 1 Y'
        ];

        $this->assertEquals($reference, $result);
    }
}
