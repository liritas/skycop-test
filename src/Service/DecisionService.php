<?php
// src/Service/DecisionService.php
namespace App\Service;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DecisionService
{
    private $decisionMatrix;

    public function parseCSV($path) {
        // decoding CSV contents
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        $rootDir = dirname(__DIR__);
        if (!file_exists($path)) {
            if (file_exists($rootDir.'/..'.$path)) {
                $path = $rootDir.'/..'.$path;
            } else {
                return "File $path not exists.";
            }
        }
        $flights = $serializer->decode(file_get_contents($path), 'csv');
        $decisionTable = $serializer->decode(file_get_contents($rootDir.'/../data/decision_table.csv'), 'csv');
        $euCountries = $serializer->decode(file_get_contents($rootDir.'/../data/eu_countries.csv'), 'csv');
        // trim array data
        $this->trim_array($flights);
        $this->trim_array($decisionTable);
        $this->trim_array($euCountries);
        $euCountries = array_column($euCountries, 'ISO');

        // convert .csv decision table to matrix
        $conditions = [];
        $rules = [];
        foreach ($decisionTable as $line) {
            foreach($line as $key => $value) {
                switch ($key) {
                    case 'Сondition':
                        if ($value === 'Claimable') {
                            $this->decisionMatrix = [];
                        } else {
                            $conditions[] = $this->getConditionFromExpression($value);
                        }
                        break;
                    default:
                        if (isset($this->decisionMatrix)) {
                            $ruleValues = $rules[$key];
                            $this->decisionMatrix[$key] = [
                                'rules' => $ruleValues,
                                'result' => $value
                            ];
                            break;
                        }
                        if (!isset($rules[$key])) {
                            $rules[$key] = [];
                        }
                        if ($value === 'Y' || $value === 'N') {
                            $rules[$key][] = $value;
                        } else {
                            $rules[$key][] = '*';
                        }
                        break;
                }
            }
        }

        // calculate results
        $results = [];
        foreach ($flights as $flight) {
            $iso = $flight['ISO'];
            $status = $flight['Status'];
            $details = $flight['Details'];
            $ruleValues = [];

            foreach ($conditions as $condition) {
                if (eval('return '.$condition.';')) {
                    $ruleValues[] = 'Y';
                } else {
                    $ruleValues[] = 'N';
                }
            }

            $result = $this->check_rules($ruleValues);
            $results[] = "$iso $status $details $result";
        }

        return $results;
    }
    
    private function trim_array(&$array)
    {
        foreach ($array as &$item) {
            $item = array_map('trim', $item);
        }
    }

    private function check_rules($ruleValues)
    {
        foreach ($this->decisionMatrix as $decision) {
            $rules = $decision['rules'];
            if ($this->is_same_arrays($ruleValues, $rules)) {
                return $decision['result'];
            }
        }
    }

    private function is_same_arrays($array1, $array2)
    {
        foreach ($array1 as $key => $value1) {
            if ($value1 !== '*' && $array2[$key] !== '*' && $value1 !== $array2[$key]) {
                return false;
            }
        }

        return true;
    }

    private function getConditionFromExpression($expression) {
        switch ($expression) {
            case 'Departure from EU':
                return 'in_array($iso, $euCountries)';
            case 'Cancelled <=14 days':
                return '$status === "Cancel" && $details <= 14';
            case 'Departure delay >=3 h':
                return '$status === "Delay" && $details >= 3';
        }
    }
}
