<?php
// src/Command/ParseCsvCommand.php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\DecisionService;

class ParseCsvCommand extends Command
{
    private $decisionService;

    public function __construct(DecisionService $DecisionService)
    {
        $this->decisionService = $DecisionService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:parse-csv')
            ->setDescription('Parse csv file through decision table.')
            ->setHelp(
                'This command reads a .csv file and return information for every flight whether flight is claimable. '.
                'Please run `php bin/console app:parse-csv [path to CSV data file]` to execute command and view results.'
            )
            ->addArgument('file-path', InputArgument::REQUIRED, 'Path to the .csv file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('file-path');
        $results = $this->decisionService->parseCSV($path);
        $output->writeln($results);
    }

}
